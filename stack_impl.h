#include <stddef.h>

#define STACK_INIT_CAPACITY 4

struct stack {
  const char *_whoami; /* NOTE: for DEBUG output */
  size_t capacity;
  size_t count;
  size_t item_sz;
  void *items;
};

int stack_init(struct stack*, size_t item_sz);
int stack_push(struct stack*, void *item);
void *stack_pop(struct stack*);
void stack_free(struct stack*);
int stack_ensure_capacity(struct stack*, size_t new_count);

