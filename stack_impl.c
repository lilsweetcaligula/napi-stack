#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "stack_impl.h"

int stack_init(struct stack* st, size_t item_sz) {
  assert(st);

  st->_whoami = "yesiamastack";
  st->capacity = STACK_INIT_CAPACITY;
  st->count = 0;
  st->item_sz = item_sz;
  st->items = malloc(STACK_INIT_CAPACITY * item_sz);

  if (!st->items) {
    return -1;
  }

  return 0;
}

int stack_push(struct stack* st, void *item_ptr) {
  assert(st);

  if (stack_ensure_capacity(st, st->count + 1)) {
    return -1;
  }

  void *dest = ((char*) st->items) + st->item_sz * st->count;
  memcpy(dest, item_ptr, st->item_sz);

  st->count++;

  return 0;
}

void *stack_pop(struct stack *st) {
  assert(st);
  assert(st->count > 0);

  st->count--;

  void *item_ptr = ((char*) st->items) + st->item_sz * st->count;

  return item_ptr;
}

int stack_ensure_capacity(struct stack* st, size_t new_count) {
  assert(st);

  if (new_count <= st->capacity) {
    return 0;
  }

  size_t new_capa = st->capacity * 2 > new_count
    ? st->capacity * 2
    : new_count;

  void *new_mem = realloc(st->items, new_capa * st->item_sz);

  if (!new_mem) {
    return -1;
  }

  st->items = new_mem;
  st->capacity = new_capa;

  return 0;
}

void stack_free(struct stack* st) {
  assert(st);

  st->_whoami = NULL;
  st->capacity = 0;
  st->count = 0;
  st->item_sz = 0;

  free(st->items); st->items = NULL;
}
