#include <node_api.h>

#include "stack_impl.h"


napi_value Init(napi_env env, napi_value exports);

napi_status DefineMethod(
  napi_env env,
  napi_value obj,
  const char *method_name,
  napi_callback method,
  void *data,
  napi_value *result
);

void AssertNapiOk(napi_env, napi_status);
napi_status BoxValue(napi_env env, napi_value value, napi_value *out_box);
napi_status UnboxValue(napi_env env, napi_value box, napi_value *out_value);

napi_value StackNew(napi_env env, napi_callback_info info);
napi_value StackPush(napi_env env, napi_callback_info info);
napi_value StackPop(napi_env env, napi_callback_info info);
napi_value StackSize(napi_env env, napi_callback_info info);
void StackFinalize(napi_env env, void *stack, void *hint);

NAPI_MODULE(native_stack, Init);
