#include <stdio.h>
#include <stdarg.h>

#define DEBUG_LOGGING_ENABLED 0

void DebugLog(const char *fmt, ...) {
  if (!(DEBUG_LOGGING_ENABLED)) {
    return;
  }

  va_list args;
  va_start(args, fmt);

  printf("DEBUG: ");
  vprintf(fmt, args);
  putchar('\n');

  va_end(args);
}

