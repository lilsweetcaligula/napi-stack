#include <node_api.h>

#include <string.h>
#include <stdlib.h>

#include "native_stack.h"
#include "debug_log.h"


napi_value Init(napi_env env, napi_value exports) {
  napi_status status = napi_ok;

  status = napi_create_function(env, NULL, 0, StackNew, NULL, &exports);

  if (status != napi_ok) {
    napi_throw_error(env, NULL, "napi_create_function");
  }

  return exports;
}


napi_value StackNew(napi_env env, napi_callback_info info) {
  DebugLog("StackNew");


  napi_status status = napi_ok;
  napi_value this = NULL;
  napi_value managed_stack = NULL;


  status = napi_get_cb_info(env, info, NULL, NULL, &this, NULL);

  if (status != napi_ok) {
    napi_throw_error(env, NULL, "napi_get_cb_info");
  }


  /* BEGIN: init the native stack
   */

  struct stack *st = calloc(1, sizeof(*st));

  if (!st) {
    napi_throw_error(env, NULL, "calloc");
  }

  if (stack_init(st, sizeof(napi_ref))) {
    napi_throw_error(env, NULL, "stack_init");
  }

  status = napi_create_external(env, st, StackFinalize, NULL, &managed_stack);

  if (status != napi_ok) {
    napi_throw_error(env, NULL, "napi_create_external");
  }

  /* END: init the native stack
   */


  /* BEGIN: define the stack's interface
   */

  DefineMethod(env, this, "push", StackPush, st, NULL);
  DefineMethod(env, this, "pop", StackPop, st, NULL);
  DefineMethod(env, this, "size", StackSize, st, NULL);

  /* END: define the stack's interface
   */


  return NULL;
}

void StackFinalize(napi_env env, void *stack, void *hint) {
  DebugLog("StackFinalize");

  DebugLog("StackFinalize: stack capa before dealloc'ed: %zd\n",
    ((struct stack*) stack)->capacity);

  /* TODO: delete the remaining references in the stack. */

  stack_free((struct stack*) stack);
  free(stack);
}

napi_value StackPush(napi_env env, napi_callback_info info) {
  DebugLog("StackPush");

  napi_status status = napi_ok;
  size_t argc = 1;
  napi_value argv[1] = { 0 };
  void *data = NULL;

  status = napi_get_cb_info(env, info, &argc, argv, NULL, &data);

  if (status != napi_ok) {
    napi_throw_error(env, NULL, "napi_get_cb_info");
  }

  if (!data) {
    napi_throw_error(env,
      NULL, "Expected napi_get_cb_info to set the stack address.");
  }
  

  struct stack *st = data;
  napi_value arg = argv[0];


  napi_value boxed_arg = NULL;
  status = BoxValue(env, arg, &boxed_arg);

  if (status != napi_ok) {
    napi_throw_error(env, NULL, "BoxValue");
  }


  DebugLog("StackPush: verifying it's a stack: %s", st->_whoami);

  DebugLog("StackPush: stack capa before push: %zd\n",
    ((struct stack*) st)->capacity);


  napi_ref arg_ref = NULL;
  status = napi_create_reference(env, boxed_arg, 1, &arg_ref);

  if (status != napi_ok) {
    napi_throw_error(env, NULL, "napi_create_reference");
  }


  if (stack_push(st, &arg_ref)) {
    napi_throw_error(env, NULL, "stack_push");
  }


  return NULL;
}

napi_value StackPop(napi_env env, napi_callback_info info) {
  DebugLog("StackPop");

  napi_status status = napi_ok;
  void *data = NULL;

  status = napi_get_cb_info(env, info, NULL, NULL, NULL, &data);

  if (status != napi_ok) {
    napi_throw_error(env, NULL, "napi_get_cb_info");
  }

  if (!data) {
    napi_throw_error(env,
      NULL, "Expected napi_get_cb_info to set the stack address.");
  }

  struct stack *st = data;

  DebugLog("StackPop: verifying it's a stack: %s", st->_whoami);

  napi_ref *ref = stack_pop(st);
  napi_value boxed_value = NULL;

  status = napi_get_reference_value(env, *ref, &boxed_value);

  if (status != napi_ok) {
    napi_throw_error(env, NULL, "napi_get_reference_value");
  }

  napi_value value = NULL;
  status = UnboxValue(env, boxed_value, &value);

  if (status != napi_ok) {
    napi_throw_error(env, NULL, "UnboxValue");
  }

  /* TODO: delete the reference. */

  return value;
}

napi_value StackSize(napi_env env, napi_callback_info info) {
  DebugLog("StackSize");

  napi_status status = napi_ok;
  void *data = NULL;

  status = napi_get_cb_info(env, info, NULL, NULL, NULL, &data);

  if (status != napi_ok) {
    napi_throw_error(env, NULL, "napi_get_cb_info");
  }

  if (!data) {
    napi_throw_error(env,
      NULL, "Expected napi_get_cb_info to set the stack address.");
  }

  struct stack *st = data;

  DebugLog("StackSize: verifying it's a stack: %s", st->_whoami);

  napi_value st_size = NULL;

  status = napi_create_uint32(env, st->count, &st_size);

  if (status != napi_ok) {
    napi_throw_error(env, NULL, "napi_create_uint32");
  }

  return st_size;
}

napi_status DefineMethod(
  napi_env env,
  napi_value obj,
  const char *method_name,
  napi_callback method,
  void *data,
  napi_value *result
) {
  napi_status status = napi_ok;
  napi_value js_method_name = NULL;
  napi_value js_method = NULL;

  status = napi_create_string_utf8(env, method_name,
    NAPI_AUTO_LENGTH, &js_method_name);

  if (status != napi_ok) {
    return status;
  }

  status = napi_create_function(env, method_name,
    NAPI_AUTO_LENGTH, method, data, &js_method);

  if (status != napi_ok) {
    return status;
  }

  status = napi_set_property(env, obj, js_method_name, js_method);

  if (status != napi_ok) {
    return status;
  }

  if (result) {
    *result = js_method;
  }

  return status;
}

void AssertNapiOk(napi_env env, napi_status status) {
  if (status == napi_ok) return;

  const napi_extended_error_info *error_info = NULL;

  if (napi_ok == napi_get_last_error_info(env, &error_info)) {
    napi_throw_error(env, NULL, error_info->error_message);
  }

  napi_throw_error(env, NULL, "failed to fetch the N-API error info");
}

napi_status BoxValue(napi_env env, napi_value value, napi_value *out_box) {
  napi_value wrapper = NULL;
  AssertNapiOk(env, napi_create_object(env, &wrapper));


  napi_value prop_name = NULL;

  AssertNapiOk(env, napi_create_string_utf8(env, "value$",
    NAPI_AUTO_LENGTH, &prop_name));


  AssertNapiOk(env, napi_set_property(env, wrapper, prop_name, value));

  
  if (!out_box) {
    napi_throw_error(env, NULL, "Expected the \"out_box\" param to be specified.");
  }


  *out_box = wrapper;

  return napi_ok;
}

napi_status UnboxValue(napi_env env, napi_value box, napi_value *out_value) {
  if (!out_value) {
    napi_throw_error(env, NULL, "Expected the \"out_value\" param to be specified.");
  }

  napi_value prop_name = NULL;

  AssertNapiOk(env, napi_create_string_utf8(env, "value$",
    NAPI_AUTO_LENGTH, &prop_name));


  AssertNapiOk(env, napi_get_property(env, box, prop_name, out_value));

  return napi_ok;
}

