### Native stack implementation in C for Node.js via N-API

It's just me experimenting with the N-API. That's all.


### How to run

Clone the repo, cd into the folder and run the command:
```
$ npm install && node .
```
Once run, the code will compile and run the Node.js app(let).


### Stack interface

The stack is required in the Node.js code like so:
```
const Stack = require('native_stack')
```

The stack exposes the following public interface to a Node.js client::

#### constructor
Call signature: `constructor()`
Example:
```
const stack = new Stack()
```

#### push
Call signature: `Stack#push(value: any): undefined`
Example:
```
const stack = new Stack()
stack.push(37)
```

#### pop
Call signature: `Stack#pop(): any`
Example:
```
const stack = new Stack()
stack.push('foo')
const s = stack.pop()

console.log(s)
```

#### size
Call signature: `Stack#size(): Number`
Example:
```
const stack = new Stack()

while (stack.size() < 5) {
  const r = Math.random()
  stack.push(r)
}
```

