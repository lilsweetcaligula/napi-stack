const bindings = require('bindings')
const NativeStack = bindings('native_stack')

const app = () => {
  const stack = new NativeStack()
  const items = [37, 'abc', null, 123, 'lorem ipsum dolorem sit amet']

  for (const item of items) {
    stack.push(item)
  }

  while (stack.size() > 0) {
    console.log(stack.pop())
  }

  console.log('ok')
}

app()
