#include <stdio.h>
#include "stack_impl.h"

int main(void) {
  const char *quotes[] = {
    "alea iacta est",
    "docendo discitur",
    "gesta non verba",
    "hic sunt leones",
    "lorem ipsum",
    NULL
  };

  struct stack st = { 0 };

  if (stack_init(&st, sizeof(char*))) {
    return -1;
  }

  for (int times = 1; times <= 99; times++) {
    for (const char **quote_ptr = quotes; *quote_ptr; quote_ptr++) {
      if (stack_push(&st, quote_ptr)) {
        perror("stack_push: ");
      }
    }
  }

  while (st.count > 0) {
    const char* item = *((char**) stack_pop(&st));
    printf("%s\n", item);
  }

  stack_free(&st);

  puts("done");

  return 0;
}
